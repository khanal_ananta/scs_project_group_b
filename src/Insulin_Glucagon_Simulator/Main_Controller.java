/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Insulin_Glucagon_Simulator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 *this is the main class from where program starts
 * @author ananta
 */
public class Main_Controller extends Application {
    
    
    
    
    //function to start the login page
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        stage.setTitle("Login page");
    }

    
    
    /**
     * @param args the command line arguments
     * main function : program start from here
     */
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
