/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Insulin_Glucagon_Simulator;

import java.net.URL;
import java.util.ResourceBundle;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ananta
 */
public class Math_ModelIT {
    
    public Math_ModelIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIniGlu method, of class Math_Model.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetIniGlu() throws Exception {
        System.out.println("getIniGlu");
        double ig = 80;
        Math_Model instance = new Math_Model();
        instance.getIniGlu(ig);
        assertEquals(80,ig);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModes method, of class Math_Model.
     */
    @Test
    public void testGetModes() throws Exception {
        System.out.println("getModes");
        String mode_1 = "automatic";
        Math_Model instance = new Math_Model();
        instance.getModes(mode_1);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getActivities method, of class Math_Model.
     */
    @Test
    public void testGetActivities() throws Exception {
        System.out.println("getActivities");
        String acti = "jogging";
        Math_Model instance = new Math_Model();
        instance.getActivities(acti);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of bslCalc method, of class Math_Model.
     */
    @Test
    public void testBslCalc() throws Exception {
        System.out.println("bslCalc");
        Math_Model instance = new Math_Model();
        instance.bslCalc();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of check_mode method, of class Math_Model.
     */
    @Test
    public void testCheck_mode() throws Exception {
        System.out.println("check_mode");
        String m = "";
        Math_Model instance = new Math_Model();
        instance.check_mode(m);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of bsl_check method, of class Math_Model.
     */
    @Test
    public void testBsl_check() throws Exception {
        System.out.println("bsl_check");
        Math_Model instance = new Math_Model();
        instance.bsl_check();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of bsl_check_man method, of class Math_Model.
     */
    @Test
    public void testBsl_check_man() throws Exception {
        System.out.println("bsl_check_man");
        Math_Model instance = new Math_Model();
        instance.bsl_check_man();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of normal method, of class Math_Model.
     */
    @Test
    public void testNormal() {
        System.out.println("normal");
        Math_Model instance = new Math_Model();
        instance.normal();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of high method, of class Math_Model.
     */
    @Test
    public void testHigh() throws Exception {
        System.out.println("high");
        Math_Model instance = new Math_Model();
        instance.high();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of low method, of class Math_Model.
     */
    @Test
    public void testLow() throws Exception {
        System.out.println("low");
        Math_Model instance = new Math_Model();
        instance.low();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of verylow method, of class Math_Model.
     */
    @Test
    public void testVerylow() throws Exception {
        System.out.println("verylow");
        Math_Model instance = new Math_Model();
        instance.verylow();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculate_bsl1 method, of class Math_Model.
     */
    @Test
    public void testCalculate_bsl1() throws Exception {
        System.out.println("calculate_bsl1");
        Math_Model instance = new Math_Model();
        instance.calculate_bsl1();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculate_bsl2 method, of class Math_Model.
     */
    @Test
    public void testCalculate_bsl2() throws Exception {
        System.out.println("calculate_bsl2");
        Math_Model instance = new Math_Model();
        instance.calculate_bsl2();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculate_bsl3 method, of class Math_Model.
     */
    @Test
    public void testCalculate_bsl3() throws Exception {
        System.out.println("calculate_bsl3");
        Math_Model instance = new Math_Model();
        instance.calculate_bsl3();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of initialize method, of class Math_Model.
     */
    @Test
    public void testInitialize() {
        System.out.println("initialize");
        URL location = null;
        ResourceBundle resources = null;
        Math_Model instance = new Math_Model();
        instance.initialize(location, resources);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
