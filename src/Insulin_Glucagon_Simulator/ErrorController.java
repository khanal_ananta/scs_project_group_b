/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Insulin_Glucagon_Simulator;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 * this class handle error and manual mode
 * @author ananta controls all the error and manual mode related message display
 */
public class ErrorController implements Initializable {

    @FXML
    private Label error;

    @FXML
    private Label manual;

    @FXML
    public Button backButton;

    @FXML
    public Button reset;

    /**
     * Initializes the controller class.
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Main_GUI_Controller main = new Main_GUI_Controller();

    }

    
    
    //pass the error string or message to gui
    public void showError(String errorMessage) {
        error.setText(errorMessage);
    }

    
    
    
    //function to control all the error string 
    public void displayError(String errorstring) throws IOException {

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("error.fxml"));
        Parent root = (Parent) loader.load();

        ErrorController eController = loader.getController();
        eController.showError(errorstring);

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
        stage.setTitle(" Important Notifications");
    }
    
    

    //function to control the manual mode strings
    public void display_manual(String errorstring) throws IOException {

        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("error.fxml"));
        Parent root = (Parent) loader.load();

        ErrorController eController = loader.getController();
        eController.showError(errorstring);

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
        stage.setTitle(" Manual mode");
    }

    
    
    
    //button for going back
    @FXML
    public void handleBackButtonAction(ActionEvent event) {
        Main_GUI_Controller main = new Main_GUI_Controller();

        Stage stage = (Stage) backButton.getScene().getWindow();

        stage.close();
        
       

    }

}
