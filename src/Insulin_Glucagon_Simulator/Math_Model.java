/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Insulin_Glucagon_Simulator;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.util.Duration;
import static javafx.util.Duration.seconds;


/**
 *this class describe the mathematical model
 * @author ananta
 */
public class Math_Model implements Initializable {

    // instance of main controller
    Main_GUI_Controller m1;

  
    
    public Math_Model(Main_GUI_Controller m1) {
        this.m1 = m1;
    }

    
    
    // instance of status class
    status s1;

    public Math_Model(status s1) {
        this.s1 = s1;
    }

    
    
    // instance of error controller class
    ErrorController error2 = new ErrorController();

    
    
    // function to pass the initail glucose value from main controller to here
    Math_Model() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
    //function to pass initial value of glucose to math_model controller
    public double ini_val;

    public void getIniGlu(double ig) throws Exception {

        ini_val = ig;

    }

    
    
    
    //function to pass initial value of mode to math_model controller
    public String fmode;

    public void getModes(String mode_1) throws Exception {

        fmode = mode_1;

    }

    
    
    
    //function to get activities from main controller
    public double carbohydrateMeal;
    public String acti;

    public void getActivities(String acti) throws Exception {

        if (null == acti) {

        } else {
            switch (acti) {
                case "before meal":
                    carbohydrateMeal = 0.00;
                    bslCalc();
                    break;
                case "after meal":
                    carbohydrateMeal = 100.00;
                    bslCalc();
                    break;
                case "bedtime":
                    carbohydrateMeal = 50.00;
                    bslCalc();
                    break;
                case "jogging":
                    carbohydrateMeal = -50.00;
                    bslCalc();
                    break;
                case "fasting":
                    carbohydrateMeal = -25.00;
                    bslCalc();
                    break;
                default:
                    break;
            }
        }

    }
    

    /**
     * mathematical model for insulin glucagon pump function to calculate the
     * blood sugar level we are using Pharmacokinetic pharmacodynamics (PK/PD)
     * model since through matlab after analysing its equation there is change
     * the blood sugar level in between t=10 to t=30 so we are taking only t=10
     * to t=30
     */
    public double bg_at_time;

    public void bslCalc() throws Exception {

        int t1 = 10;
        int t2 = 20;
        int t3 = 30;
        double k1 = 0.0453;                   //  Glycemic Index (constant) 
        double k2 = 0.0224;                   // Rate at which insulin is released(Insulin Sensitivity)

        double bg_at_timet1 = Math.round((carbohydrateMeal * (k1 / (k2 - k1)))
                * (Math.exp(-k1 * t1) - Math.exp(-k2 * t1)) + ini_val);

        double bg_at_timet2 = Math.round((carbohydrateMeal * (k1 / (k2 - k1)))
                * (Math.exp(-k1 * t2) - Math.exp(-k2 * t2)) + ini_val);

        double bg_at_timet3 = Math.round((carbohydrateMeal * (k1 / (k2 - k1)))
                * (Math.exp(-k1 * t3) - Math.exp(-k2 * t3)) + ini_val);

        if (bg_at_timet1 > 120.00) {

            bg_at_time = bg_at_timet1;
            check_mode(fmode);

        } else if (bg_at_timet2 > 120.0) {

            bg_at_time = bg_at_timet2;
            check_mode(fmode);
        } else if (bg_at_timet3 > 120.0) {

            bg_at_time = bg_at_timet3;
            check_mode(fmode);
        } else if (bg_at_timet1 < 72.0) {

            bg_at_time = bg_at_timet2;
            check_mode(fmode);
        } else if (bg_at_timet1 < 72.0) {

            bg_at_time = bg_at_timet2;
            check_mode(fmode);
        } else if (bg_at_timet3 < 72.0) {

            bg_at_time = bg_at_timet2;
            check_mode(fmode);
        } else {

            bg_at_time = bg_at_timet1;
            check_mode(fmode);
        }

    }
    
    

    //function  to check operating mode
    public void check_mode(String m) throws Exception {

        if (null == m) {

            error2.displayError("enter mode");
        } else {
            switch (m) {
                case "automatic":
                    bsl_check();
                    break;
                case "manual":

                    bsl_check_man();
                    break;
                default:
                    error2.displayError("enter mode");
                    break;
            }
        }

    }
    
    

    // function to check the blood suger level high,low, very low and normal
    public double difference;

    public void bsl_check() throws Exception {

        if (72.00 <= bg_at_time && bg_at_time <= 120.00) {
            normal();
            error2.displayError("your blood sugar level is normal");

        } else if (bg_at_time > 120.00) {
            difference = bg_at_time - 90.00;
            error2.displayError("high sugar level -insulin injected");
            high();
            

        } else if (bg_at_time < 72.00 && bg_at_time >= 50.00) {
            difference = 90 - bg_at_time;
            error2.displayError("low blood sugar level- glucagon injected");
            low();
            
        } else {

            difference = 90 - bg_at_time;
            error2.displayError("be careful very low blood sugar level-glucagon injected");
            verylow();
            
            
        }
    }
    
    
    

    
    //blood sugar check in manual mode
    public void bsl_check_man() throws Exception {

        if (72.00 <= bg_at_time && bg_at_time <=120.00) {
            
            error2.display_manual("your blood sugar level is normal");
            this.m1.BSL.setText(Double.toString(bg_at_time));
            
            this.m1.normal.setFill(javafx.scene.paint.Color.GREEN);
            this.m1.high.setFill(javafx.scene.paint.Color.WHITE);
            this.m1.low.setFill(javafx.scene.paint.Color.WHITE);
            this.m1.alram.setFill(javafx.scene.paint.Color.WHITE);
            this.m1.h1.setText(Double.toString(bg_at_time));
        } 
        
        
        
        else if (bg_at_time > 120.00) {
            difference = bg_at_time - 90.00;
            if (difference < 25) {
                insulin = .05;
                error2.display_manual("high bsl: please inject .5 unit of insulin");

            } else if (difference >= 2500) {
                insulin = .5;
                double new_difference = difference - 2500;
                new_insulin = new_difference / 50;
                double insulin_unit = new_insulin ;

                error2.display_manual("high blood sugar level: please inject one does of 25 units and another of" + insulin_unit + "unit ");

            } else {
                insulin = difference / insulin_sensitivity;
                
                error2.display_manual("high blood sugar level: please inject " + insulin + "unit of insulin");

            }
                this.m1.normal.setFill(javafx.scene.paint.Color.WHITE);
                this.m1.high.setFill(javafx.scene.paint.Color.BLACK);
                this.m1.low.setFill(javafx.scene.paint.Color.WHITE);
                this.m1.alram.setFill(javafx.scene.paint.Color.WHITE);
                this.m1.BSL.setText(Double.toString(bg_at_time));
                this.m1.h2.setText(Double.toString(bg_at_time));
        } 
        
        
        else if (bg_at_time < 72.00 && bg_at_time >= 50.00) {
            difference = 90 - bg_at_time;
            if (difference < 25) {
                
                
                error2.displayError("low blood sugar level: please inject.5 unit of glucagon");
            } else {
                glucagon = difference / glucagon_sensitivity;
                double glucagon_unit=glucagon;
                error2.displayError("low blood sugar level: please inject " + glucagon_unit + "unit of glucagon");
                

            }
              this.m1.normal.setFill(javafx.scene.paint.Color.WHITE);
              this.m1.high.setFill(javafx.scene.paint.Color.WHITE);
              this.m1.low.setFill(javafx.scene.paint.Color.RED);
              this.m1.alram.setFill(javafx.scene.paint.Color.WHITE);
              this.m1.BSL.setText(Double.toString(bg_at_time));
              this.m1.h3.setText(Double.toString(bg_at_time));
        } 
        
        
        
        else {

            difference = 90 - bg_at_time;
            glucagon = difference / glucagon_sensitivity;
                double glucagon_unit1=glucagon;
                error2.displayError("very low blood sugar level: please inject " + glucagon_unit1 + "unit");
                this.m1.alram.setFill(javafx.scene.paint.Color.RED);
                this.m1.low.setFill(javafx.scene.paint.Color.RED);
                this.m1.normal.setFill(javafx.scene.paint.Color.WHITE);
                this.m1.high.setFill(javafx.scene.paint.Color.WHITE);
                this.m1.BSL.setText(Double.toString(bg_at_time));
                this.m1.h4.setText(Double.toString(bg_at_time));
        }
        
    }
    
    
    

    //function to manupulate the low blood suger level
    public void normal() {

        this.m1.normal.setFill(javafx.scene.paint.Color.GREEN);
        this.m1.high.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.low.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.alram.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.BSL.setText(Double.toString(bg_at_time));
        this.m1.h1.setText(Double.toString(bg_at_time));
        this.m1.plotGraph(bg_at_time);
    }
    
    

    //function to manupulate high blood suger level 
    //insulin sensitivity for normal person is 50
    public double insulin;
    public double insulin_sensitivity = 50;
    public double new_insulin;

    public void high() throws Exception {

        if (difference < 25) {
            insulin = .05;
            this.m1.pb2.setProgress(1.0 - insulin);
            this.m1.pi2.setProgress(1.0 - insulin);
        } else if (difference >= 2500) {
            insulin = .25;

            this.m1.pb2.setProgress(1.0 - insulin);
            this.m1.pi2.setProgress(1.0 - insulin);
            double new_difference = difference - 2500;
            new_insulin = new_difference / 50;
            this.m1.pb2.setProgress(1.0 - insulin - (new_insulin / 100));
            this.m1.pi2.setProgress(1.0 - insulin - (new_insulin / 100));
        } else {
            insulin = difference / insulin_sensitivity;

            this.m1.pb2.setProgress(1.0 - (insulin / 100));
            this.m1.pi2.setProgress(1.0 - (insulin / 100));

        }

        calculate_bsl1();
       this.m1.plotGraph(bg_at_time);
    }

    
    
    
    //function to manupulate low blood suger level
    public double glucagon;
    public double glucagon_sensitivity = 50;

    public void low() throws Exception {

        if (difference < 25) {
            glucagon = .05;
            this.m1.pb3.setProgress(1.0 - glucagon);
            this.m1.pi3.setProgress(1.0 - glucagon);
        } else {
            glucagon = difference / glucagon_sensitivity;

            this.m1.pb3.setProgress(1.0 - (glucagon / 100));
            this.m1.pi3.setProgress(1.0 - (glucagon / 100));

        }

        calculate_bsl2();
       this.m1.plotGraph(bg_at_time);
    }
    
    
    

//function to manupulate very low blood suger level    
    public void verylow() throws Exception {

        if (difference < 25) {
            glucagon = .05;
            this.m1.pb3.setProgress(1.0 - glucagon);
            this.m1.pi3.setProgress(1.0 - glucagon);
        } else {
            glucagon = difference / glucagon_sensitivity;

            this.m1.pb3.setProgress(1.0 - (glucagon / 100));
            this.m1.pi3.setProgress(1.0 - (glucagon / 100));

        }

        calculate_bsl3();
        this.m1.plotGraph(bg_at_time);  //call graph plot function
    }

    
    
    
    //function to set values if bsl is high
    public void calculate_bsl1() throws Exception {
        double d = difference / 4;
        
        this.m1.normal.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.high.setFill(javafx.scene.paint.Color.BLACK);
        this.m1.low.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.alram.setFill(javafx.scene.paint.Color.WHITE);
        
        this.m1.h2.setText(Double.toString(bg_at_time));
        this.m1.BSL.setText(Double.toString(bg_at_time - 4 * d));
        
    }
    
    
    
    //function to set values if bsl is low
    public void calculate_bsl2() throws Exception {
        double d = difference / 4;
        
        this.m1.normal.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.high.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.low.setFill(javafx.scene.paint.Color.RED);
        this.m1.alram.setFill(javafx.scene.paint.Color.WHITE);
        
        this.m1.h3.setText(Double.toString(bg_at_time));
        this.m1.BSL.setText(Double.toString(bg_at_time + 4 * d));
        
    }

    
    
    
    //function to set values if bsl value is very low
    public void calculate_bsl3() throws Exception {
        double d = difference / 4;
        
        this.m1.normal.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.high.setFill(javafx.scene.paint.Color.WHITE);
        this.m1.low.setFill(javafx.scene.paint.Color.RED);
        this.m1.alram.setFill(javafx.scene.paint.Color.RED);
        
        this.m1.h4.setText(Double.toString(bg_at_time));
        this.m1.BSL.setText(Double.toString(bg_at_time + 4 * d));
        
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}
