/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Insulin_Glucagon_Simulator;

import java.io.IOException;
import static java.lang.Double.parseDouble;
import java.net.URL;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 * handles all the GUI components
 * @author ananta
 */
public class Main_GUI_Controller implements Initializable {

    /**
     * Initializes the controller class.
     * Different component of main GUI
     * @param url
     * @param rb
     */
    
    
    @FXML
    private ChoiceBox cb1, cb2;

    @FXML
    public TextField date, time;

    @FXML
    private TextField timer;

    @FXML
    public ProgressBar pb1, pb2, pb3;

    @FXML
    public ProgressIndicator pi1, pi2, pi3;

    @FXML
    public CheckBox ckb1, ckb2, ckb3, ckb4;

    @FXML
    public TextField iniGlucose;

    @FXML
    public Label BSL;

    @FXML
    public Label h1, h2, h3, h4;

    @FXML
    public LineChart<Number, Number> lineplot;

    @FXML
    public NumberAxis x;

    @FXML
    public NumberAxis y;

    @FXML
    public Circle low;

    @FXML
    public Circle normal;

    @FXML
    public Circle high;

    @FXML
    public Circle alram;
    
    @FXML
    public Label test;
    
    
     
    //create math object of class math_model
    Math_Model math = new Math_Model(this);

    
    //create st boject of the class status
    status st = new status(this);
    
    

    // function to display date and time
    @FXML
    public void showdate() {
        Date d = new Date();
        //SimpleDateFormat a=new SimpleDateFormat("dd-MM-yyyy");
        date.setText(d.toString());
        //date.setText("ananta");
    }
    
    

    
    // function to display timer
    Double secondPass = 0.0;

    public void showtimer() throws IOException {
        Timer myTimer;

        myTimer = new Timer();
        TimerTask task;
        task = new TimerTask() {   
            public void run() {
                try {
                    //throw new UnsupportedOperationException("Not supported yet.");

                     st.CalBatLevel(secondPass);
                     //math.Cal_bsl3(secondPass);
                } catch (IOException ex) {
                    Logger.getLogger(Main_GUI_Controller.class.getName()).log(Level.SEVERE, null, ex);
                }

                secondPass = secondPass + 1;
                String numberAsString = Double.toString(secondPass);
                timer.setText(numberAsString);
            }
        };
        myTimer.scheduleAtFixedRate(task, 1000, 1000);

    }
    
    
    
    
  
   // function to take the initial values for battery
    public double inivalueB;
    public void ini_levelb(double l){
       
       inivalueB = l;
   }
   
   
   // function to take the initial values for insulin
   public double inivalueI;
   public void ini_leveli(double l){
       
       inivalueI = l;
   }
   
   
   
   // function to take the initial values for glucagon
   public double inivalueG;
   public void ini_levelg(double l){
       
       inivalueG = l;
   }
    

   
    // function to take initial  glucose value
    public double bg_ini;
    public void getInitialGlucose() throws Exception {

        bg_ini = Double.parseDouble(iniGlucose.getText()); //take initail blood suger level from gui
        this.math.getIniGlu(bg_ini);
        
    }
    
    
    

    // function of take activitie
    public String activity;
    public void getActivities() throws Exception {
        activity = cb1.getValue().toString();
        this.math.getActivities(activity);
        
    }
  
    
    
    // function of take activitie
    public String mode;
    public void getMode() throws Exception {
        mode = cb2.getValue().toString();
        
       this.math.getModes(mode);
    }
        
    

    // function for button start
    @FXML
    public void start(ActionEvent highdata) throws Exception {

        boolean b1, b2, b3, b4;

        b1 = checkIniHardware();
        b2 = checkIniValueB(inivalueB);
        b3 = checkIniValueI(inivalueI);
        b4 = checkIniValueG(inivalueG);

        if (b1 == true && b2 == true && b3 == true && b4 == true) {

            checkIniHardware();  //check initial hardware status
            showtimer();                //start timer
            getInitialGlucose();   // take initial glucose value
            getMode();
            getActivities();      // get activites
            

          //Thread.sleep(2000);
            

        }

    }
    
    
    
    

    //function for button recharge
    @FXML
    public void recharge(ActionEvent recharge) throws Exception {

        pb1.setProgress(1.0); // set battery to full
        pi1.setProgress(1.0);// set battery to full
        ini_levelb(1.0);
    }
    
    
    

    //function for insuline refill
    @FXML
    public void irefill(ActionEvent irefill) throws Exception {

        pb2.setProgress(1.0);   // set insulin to full
        pi2.setProgress(1.0);    // set glucagon to full
        ini_leveli(1.0);
    }
    
    
    

    // function for glucagon refill
    @FXML
    public void grefill(ActionEvent grefill) throws Exception {

        pb3.setProgress(1.0);      // set glucagon to full
        pi3.setProgress(1.0);       // set glucagon to full
        ini_levelg(1.0);
    }
    
    
    

    // function to check initial value of Battery
    @FXML
    public boolean checkIniValueB(double b) throws IOException {
        if (b <= 0.3) {

            ErrorController er;
            er = new ErrorController();
            er.displayError("battery level is very low  please recharge");
            return false;
        } else {
            return true;
        }

    }
    
    
    
    // function to check initial value of Insulin
    @FXML
    public boolean checkIniValueI(double i) throws IOException {

        if (i <= 0.3) {

            ErrorController er;
            er = new ErrorController();
            er.displayError("insuline level is very low please refill");
            return false;
        } else {
            return true;
        }

    }

    
    // function to check initial value of Glucagon
    @FXML
    public boolean checkIniValueG(double g) throws IOException {
        if (g <= 0.3) {

            ErrorController er;
            er = new ErrorController();
            er.displayError("glucagon level is very low please refill");
            return false;
        } else {
            return true;
        }

    }
    
    

    //function to check initial hardware status
    public boolean checkIniHardware() throws IOException {
        if (ckb1.isSelected() && ckb2.isSelected() && ckb3.isSelected() && ckb4.isSelected()) {
            return true;
        } else {

            if (!ckb1.isSelected()) {
                ErrorController er;
                er = new ErrorController();
                er.displayError("pump not functioning");
            } else if (!ckb2.isSelected()) {
                ErrorController er;
                er = new ErrorController();
                er.displayError("needle not inserted properly");
            } else if (!ckb3.isSelected()) {
                ErrorController er;
                er = new ErrorController();
                er.displayError("Sensor not functioning");
            } else if (!ckb4.isSelected()) {
                ErrorController er;
                er = new ErrorController();
                er.displayError("Insulin reservoir not functioning");
            }
            return false;
        }
    }
    
    
    

    //function to plot graph
    public void plotGraph(double z) {

        XYChart.Series<Number, Number> series;
        series = new XYChart.Series<>();
        x.setLabel("time");
        y.setLabel("bsl");
        lineplot.setTitle("bsl vs time");
        lineplot.getData().removeAll(lineplot.getData());
        
        
        if (72.00 <= z && z <= 120.00)                     //if bslvalue is normal
        {
            for (int t = 1; t < 500; t++) {

                series.getData().add(new XYChart.Data<>(t, z));
                //Thread.sleep(2000);
                lineplot.getData().addAll(series);

            }
            lineplot.setAnimated(false);
            series.setName("bsl vs time");
        } else if (z > 120.00)                            //if bsl value is high
        {

            for (int t = 1; t < 500; t++, z = z - 0.5) {
                if (z > 92) {

                    series.getData().add(new XYChart.Data<>(t, z));
                    //Thread.sleep(2000);
                    lineplot.getData().addAll(series);

                } else {

                    series.getData().add(new XYChart.Data<>(t, 92.00));
                    //Thread.sleep(2000);
                    lineplot.getData().addAll(series);

                }
                 
            }
            series.setName("bsl vs time");
            lineplot.setAnimated(false);

        } else                                            //if bsl value is low  
        {
         for (int t = 1; t < 500; t++, z = z + 0.25) {
                if (z < 92) {

                    series.getData().add(new XYChart.Data<>(t, z));
                    //Thread.sleep(2000);
                    lineplot.getData().addAll(series);

                } else {

                    series.getData().add(new XYChart.Data<>(t, 92.00));
                    //Thread.sleep(2000);
                    lineplot.getData().addAll(series);

                }

            }
            series.setName("bsl vs time");
           
            lineplot.setAnimated(false);
        }

    }
    
    
    
    
    

    //function for button logout
    @FXML
    public void blogout(ActionEvent logout) throws Exception {
        
        Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        
            Scene scene = new Scene(root);
        
            stage.setScene(scene);
            stage.show();
            ((Node)(logout.getSource())).getScene().getWindow().hide();
            
 
    }
    
    
    //function to set current status
    @FXML
    public void current(ActionEvent logout) throws Exception {
        
        normal.setFill(javafx.scene.paint.Color.GREEN);
        high.setFill(javafx.scene.paint.Color.WHITE);
        low.setFill(javafx.scene.paint.Color.WHITE);
        alram.setFill(javafx.scene.paint.Color.WHITE);
 
    }
    
    
    
    

    // initializable function
    public double first_value = 1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        
        
            //to show current date and time
            showdate();
            
            
            //initial values for battery,insulin and glucagon
            ini_levelb(1.0);
            ini_leveli(1.0);
            ini_levelg(1.0);
        
        
        
            // for activities
            cb1.getItems().add("jogging");
            cb1.getItems().add("fasting");
            cb1.getItems().add("before meal");
            cb1.getItems().add("after meal");
            cb1.getItems().add("bedtime");
            
            
            //for modes
            cb2.getItems().add("automatic");
            cb2.getItems().add("manual");
            
           
            //set progress of battery
            pb1.setProgress(inivalueB);
            pi1.setProgress(inivalueB);
            
            //set progress of insulin
            pb2.setProgress(inivalueI);
            pi2.setProgress(inivalueI);
            
            //set progress of glucagon
            pb3.setProgress(inivalueG);
            pi3.setProgress(inivalueG);
            
            // initial checklist for hardware
            ckb1.setSelected(true);
            ckb2.setSelected(true);
            ckb3.setSelected(true);
            ckb4.setSelected(true);
            
            
            //setting the initial color value of status
            low.setFill(javafx.scene.paint.Color.WHITE);
            normal.setFill(javafx.scene.paint.Color.WHITE);
            high.setFill(javafx.scene.paint.Color.WHITE);
            alram.setFill(javafx.scene.paint.Color.WHITE);
            
            // setting the label field
            BSL.setStyle("-fx-border-color:black; -fx-background-color: white;");
            
            
            //setting for the history column
            h1.setStyle("-fx-border-color:black; -fx-background-color: white;");
            h2.setStyle("-fx-border-color:black; -fx-background-color: white;");
            h3.setStyle("-fx-border-color:black; -fx-background-color: white;");
            h4.setStyle("-fx-border-color:black; -fx-background-color: white;");
        

       

 }

}
