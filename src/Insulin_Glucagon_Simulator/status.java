/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Insulin_Glucagon_Simulator;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 *this gives status about battery with regards to timer
 * @author ananta
 */



public class status implements Initializable {
    
ErrorController err=new ErrorController();  //instance of errorController class
Math_Model math2;



public status(Math_Model math2){          //instance of Math_Model class
    this.math2=math2;   
}


    Main_GUI_Controller m;                    //instance of Main_GUI_Controller class
    public status(Main_GUI_Controller m){
        this.m = m;
    }
    

    
    status() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    
    
  
    
    // set progress value to our status bar of battery
    public void BValue(double blevel){
          this.m.pb1.setProgress(blevel);
         this.m.pi1.setProgress(blevel);
         //return blevel;
    }
   
    
    
    

    // calculate the decrease battery level according to time
    public void CalBatLevel(Double d) throws IOException  {
        
        
        
        for(double i=d;i<2000;i++){
        if(d>2000.0){
            
            BValue(0.1);
            
            
        }
        
        else if(d>1200.0){
            BValue(0.2);
            
            
        }
        else if(d>1050.0){
            BValue(0.3);
       
        }
        else if(d>900.0){
            BValue(0.4);
       
        }
        else if(d>750.0){
            BValue(0.5);
       
        }
        else if(d>600.0){
            BValue(0.6);
       
        }
        else if(d>450.0){
            BValue(0.7);
      
        }
        else if(d>300.0){
            BValue(0.8);
      
        }
        else if(d>200.0){
            BValue(0.9);
          
        }
             
    }
    }
    
     
    
    // initialize function
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    
    }
    
}


